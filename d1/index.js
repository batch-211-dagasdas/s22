console.log('hello world');

// Array method
// JS has built-in function and methods for arrays this allow us to manipulate and access array item

// mutator methods
/*
	-mutator Method are function that mutate or change our array after they are created
	-thse methods manipulate  the original array performing various task such as adding and removing elements

*/

let fruit = ['apple','orange','kiwi','dragon fruit'];

// .push() simple it add element at the end of an arry and return the arry length
/*
		Synttax
		arrayname.push();
*/
console.log("current array")
console.log(fruit)
 let fruitlength=fruit.push('mango');
 console.log(fruitlength);//
 console.log('mutated array from push method');
 console.log(fruit);//mango added in the end

 fruit.push('avocado','guava');
 console.log('mutated array from push method');
 console.log(fruit)

 // pop()
 /*
		this removes the last element in an array and return the removed element
		-syntax
			arrayName.pop();
 */

 let removedFruit= fruit.pop();
 console.log('mutated array from pop method');
 console.log(removedFruit);
 console.log(fruit)

 // mini activity

 let ghostFighters=['Eugene','dennis','alfred','Taguro'];
 console.log(ghostFighters)
 let unfriendFighter=ghostFighters.pop();// remove and store the removed data
 console.log("You are removed from friend list " + unfriendFighter)
 console.log(ghostFighters)

//  function unfriendFighter(){
//  	ghostFighters.pop();// remove and store the removed data
//  	console.log("You are removed from friend list " + unfriendFighter)
//  	console.log(ghostFighters)
//  }

// unfriendFighter();

 // unshift()
 /*
	-adds one or more elements at the beginning or an array

	syntax
	arrayName.unshift('elementA')
	arrayName.unshift('elementA','ElementB')
 */

 fruit.unshift('lime', 'banana')
 console.log('mutated array from unshift method');
 console.log(fruit)


// shift()
 /*
	removes an elemetn at the begginning oa an array and returns the removed element

	syntax
	arrayName.shift()

 */

 let anotherFruit=fruit.shift();
 console.log(anotherFruit);
 console.log('mutated array from shift method');
 console.log(fruit)



// splice()
 /*
	Simultaneosly removes element from the specifiend index number and add elements

	syntax
	arrayName.splice(startingIndex,deletecount,Elementstobeadded)

 */

 fruit.splice(1,2,"lime", 'cherry')
 console.log('mutated array from splice method');
 console.log(fruit)


 // sort
/*
	Rearranges the array elements in alphanumeric order
	-syntax
		arrayName.sort()
*/
	fruit.sort();
	 console.log('mutated array from sort method');
 	console.log(fruit)

 // reverse
 /*
	it reverses the order of an array elements
	-syntax
		arrayName.reverse()
 */

 	fruit.reverse();
	console.log('mutated array from reverse method');
 	console.log(fruit)


 	// Non-Mutator methds
 	/*
		Non-mutator methods are function that do not modify or change an array after the are created
		-these methods do not manipulate the original array performing various task such as returnong elements form an array and combining arrays and printing the output
 	*/

 	let countries =['us','ph','can','sg','th','ph','fr','de'];

 	// indexOf()
 	/*
		returns the index number of the 1st matching elements fount is an array
		-if no matching is found the result is -1
		-the search process will be done frm the 1st to the last element

		syntax
		arrayName/indexof(searchValue);
		arrayName/indexof(searchValue, fromIndex);
 	*/

 	let firstIndex=countries.indexOf('ph', 3);
 	console.log('result of indexOf method: ' + firstIndex)

 	let invalidCountry = countries.indexOf('br');
 	console.log('result of indexOf method: ' + invalidCountry)


 	// lastIndexOf()
 	/*
		this returns the index number of the last matching element found in an array 
		-the search process will be done from last to 1st elements
 		syntax
 		arrayName.lastIndexOf(searchValue);
 		arrayName.lastIndexOf(searchValue, fromIndex);
 	*/	

 	let lastIndex=countries.lastIndexOf('ph')
 	console.log('result of indexOf method: ' + lastIndex)


 	// slice()
 	/*
		-portions/slices elements from the array and returns a new array
		syntax
			arrayName.slice(startingindex);
			arrayName.slice(searchValue, fromIndex);
 	*/
 	// let countries =['us','ph','can','sg','th','ph','fr','de'];

 	let slicedArrayA = countries.slice(2);
 	console.log('result of slice method: ')
 	console.log(slicedArrayA)

 	let slicedArrayB = countries.slice(2,4);
 	console.log('result of slice method: ')
 	console.log(slicedArrayB)

 	let slicedArrayC = countries.slice(-3);
 	console.log('result of slice method: ')
 	console.log(slicedArrayC)

 	// toString();
 	/*
		Return an array as a string separated by commas
		syntax
		arrayName.toString();

 	*/

 	let stringArray = countries.toString();
 	console.log('result of toString method: ')
 	console.log(stringArray)


 	// concat()

 	/*
		combines 2 arrays and more and returns the combined result
		syntax
			arrayA.concat(arrayB);
			arrayA.concat(ElementA);
 	*/

//combine 2 arrays

 	let taskArrayA =['drinks html','eat Javascript'];
 	let taskArrayB =['inhale css','exhale sass'];
 	let taskArrayC =['get git','be node'];

 	let task = taskArrayA.concat(taskArrayB);
 	console.log('result of concat method: ')
 	console.log(task)

// combine 3 arrays
 	let allTask = taskArrayA.concat(taskArrayB,taskArrayC)
 	console.log('result of concat method: ')
 	console.log(allTask)


 // combine arrays w/ elements
 	let combinedTasks = taskArrayA.concat('smell express','throw react')
 	console.log('result of concat method: ')
 	console.log(combinedTasks)

//  join()
/*
	-returns an array as a string separated by a specified separator
	syntax
	arrayName.join('separatorString')
*/

let users = ['john','jane','rober','nej']
console.log(users.join())
console.log(users.join(' - '))
console.log(users.join(''))


// Iteration methods
/*
	Iteration methods are loops designed to perform repetitive task on a arrays
	Iteration methods loops over all item in an array
	useful for manipulating array data resulting in complex task
*/

// forEach
/*
	similar to a for loop that Iterate on each array elements
	-for each item in the array, the anonymous function passed in the forEach() methods will be run
	-The anonymouns is able to recieve the current item being Iterated or loop over by assigning a parameter
	-variable name for arrays are normally writen in the plura; from of the date stored in a array
	-its common parctice to use the singular frrom of the array content for parameter anmes used in array loops
	-forEach() doen not return anything

	Syntax
	arrayName.forEach(function(indivElement){
	statement
	})


*/

allTask.forEach(function(x){
 console.log(x)

})

// mini activity 2
// display ghostFighters using function
function displayFighter(){
	ghostFighters.forEach(function(display){
		console.log(display)
	});

}
displayFighter();

// using forEach w/ conditional statements

/*
	it is a good practice to print the current element in the console when wroking w/ array iteration methods to have an idea of what information is being worked on for each iteratoin of loops

	creating a separate variable to store results of an array iteration methods are also good parctice to avvoid confustion by modifying
*/

console.log(allTask)
let filteredTasks=[];
allTask.forEach(function(task){
	if(task.length>10){
		console.log(task)
		filteredTasks.push(task)
	}
})

console.log('result of filtered Tasks: ');
console.log(filteredTasks)


// map()
	/*
		-iterates on each elements and returns new array w/ diff values depending on tthe result of the function's operation
		-sybtax
		let/const resultArray=arrayName.map(function(indivElement))

	*/

	let numbers = [1,2,3,4,5];
	let numberMap=numbers.map(function(number){
		return number*number
	})

	console.log('Original Array: ')
	console.log(numbers)
	console.log('result of map method: ')
	console.log(numberMap)


	// map() vs foreach()

	let numberForEach = numbers.forEach(function(number){
		return number *number
	})
	console.log(numberForEach)

// every()
/*
	-check if all elements in an array meet given conditions
	-this is usefull for validating data stored in an arrays especially when dealling w/arge data

	syntax
	let/const resultArray=arrayname.every(function(indvelement){
	return expression/conditio
	})
*/

let allValid=numbers.every(function(number){
	return(number < 3)
})
console.log('result of every methods');
console.log(allValid);

// some()
/*
	check if at least one element in the array meets the given condition
	-return a true valaue if all least one element meets the condition and false if otherwise

	syntax:
	let/const resultArray=arrayname.some(function(indvelement){
	return expression/conditio
	})


*/

let someValid=numbers.some(function(number){
	return(number<2)
})
console.log('result of some methods');
console.log(someValid);

// combining the returned result from every/some method may be used in the other statements to perform consecutive results
if(someValid){
	console.log('some number in the array are > than 2')
}

// filter()
/*
	return a new array that contain elements w.c meets given condition
	-return an empty array if no elements wre found

	syntax:
	let/const resultArray=arrayname.filter(function(indvElement){
		return expression/condition
	})

*/

let filterValid =numbers.filter(function(number){
	return(number<3)
})
console.log('result of filter methods');
console.log(filterValid);


let nothinFound=numbers.filter(function(number){
	return(number<0)
	})
console.log('result of filter methods');
console.log(nothinFound);



// filtering using forEach

let filteredNumber=[];

numbers.forEach(function(number){
	console.log(number);

	if(number<3){
		filteredNumber.push(number);
	}
});

console.log(filteredNumber)

// include()
/*
	check if the argument passed can be found in the array
	it returns a boolean w/c can be saved in a variable
	-return true if the argument is found the array 
	-return false if its is not

	syntax
	arrayname.includes(argumenttoFind)
	
*/

let products=['Mouse','Keyboard','laptop','Monitor']

let productFound = products.includes('Mouse')
console.log(productFound)

let productNotFound = products.includes('headSet')
console.log(productNotFound)

// methods chaining
	// methods can be chained using them one after another
	// the result of the 1st method is used on the second method until all "chained" methods have been resolved


	let filterProducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	})
	console.log(filterProducts)

	// mini activity 3

	let contact = ['Ash']
	function addTrainer(trainer){
		let doesTrainerExist =contact.includes(trainer);// this create an condition

		if(doesTrainerExist){
			alert("already added in the match call")
		}else{
			contact.push(trainer);
			alert('Registered!')
		}
	}

	// reduce()
	/*
		-evaluates element from left to right and return/reduces the array into a single value
		-syntax
		let/const resultArray=arrayName.reduce(function(accumolator,currentValue){
			return expression/operation

		})

		-"accumulator" parameter in the function stores the result for every iteration of the loop
		-"currentValue" is the current/next element in the array that is evaluated in each iteration of the loop

		how the reduce method works
		1.1st result element in the array is stored in the "accumolator" parameter
		2. the 2nd/nex element in the array is stored in the currentValue parameter
		3.an operatopn is performed on the 2 elements
		4.the loop repate step 1-3
	*/


	console.log(numbers)
	let iteration = 0;
	let iterationStr=0;

	let reducedArray=numbers.reduce(function(x,y){
		console.warn('current iteration: '+ ++iteration)
		console.log('accumolator: ' + x);
		console.log('current value: ' +y)
		return x+y
	})

	console.log("result is " + reducedArray)



	let list= ['hello', 'Again', 'world']
	let reducedjoin=list.reduce(function(x,y){
		console.warn('current iteration: '+ ++iteration)
		console.log('accumolator: ' + x);
		console.log('current value: ' +y)
		return x+y
	})

	console.log("result is " + reducedjoin)


